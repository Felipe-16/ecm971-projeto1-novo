import unittest
from app import app

class testeMusicasMetodos(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def teste_listar_musicas(self):
        response = self.app.get('/musicas')
        self.assertEqual(response.status_code, 200)

    def teste_cadastrar_musica(self):
        nova_musica = {
            'nome': 'nova_musica',
            'avaliacao': 4
        }
        response = self.app.post('/musicasPost', json=nova_musica)
        self.assertEqual(response.status_code, 201)


    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
